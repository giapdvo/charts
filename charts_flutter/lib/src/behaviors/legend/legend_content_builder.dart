// Copyright 2018 the Charts project authors. Please see the AUTHORS file
// for details.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:math';

import 'package:charts_common/common.dart' as common;
import 'package:charts_common/src/common/symbol_renderer.dart';
import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart' as ui;
import 'package:flutter/widgets.dart' show BuildContext, hashValues, Widget;
import 'legend.dart';
import 'legend_entry_layout.dart';
import 'legend_layout.dart';

/// Strategy for building a legend content widget.
abstract class LegendContentBuilder {
  const LegendContentBuilder();

  Widget build(BuildContext context, common.LegendState legendState,
      common.Legend legend,
      {bool showMeasures});
}

/// Base strategy for building a legend content widget.
///
/// Each legend entry is passed to a [LegendLayout] strategy to create a widget
/// for each legend entry. These widgets are then passed to a
/// [LegendEntryLayout] strategy to create the legend widget.
abstract class BaseLegendContentBuilder implements LegendContentBuilder {
  /// Strategy for creating one widget or each legend entry.
  LegendEntryLayout get legendEntryLayout;

  /// Strategy for creating the legend content widget from a list of widgets.
  ///
  /// This is typically the list of widgets from legend entries.
  LegendLayout get legendLayout;

  @override
  Widget build(BuildContext context, common.LegendState legendState,
      common.Legend legend,
      {bool showMeasures}) {
    final entryWidgets = legendState.legendEntries.map((entry) {
      var isHidden = false;
      if (legend is common.SeriesLegend) {
        isHidden = legend.isSeriesHidden(entry.series.id);
      }

      return legendEntryLayout.build(
          context, entry, legend as TappableLegend, isHidden,
          showMeasures: showMeasures);
    }).toList();

    return legendLayout.build(context, entryWidgets);
  }
}

class WrapLegendBuilder extends LegendContentBuilder {
  final List<String> ignoringLabels;

  WrapLegendBuilder({this.ignoringLabels = const []});

  @override
  Widget build(BuildContext context, common.LegendState legendState,
      common.Legend legend,
      {bool showMeasures}) {
    return ui.Wrap(
        children: legendState.legendEntries?.map((e) {
              if (!ignoringLabels.contains(e.label)) {
                return _legendItem(e);
              } else {
                return ui.SizedBox.shrink();
              }
            })?.toList() ??
            []);
  }

  Widget _legendItem(common.LegendEntry legend) {
    final symbolRenderer = legend.symbolRenderer;

    // TODO: find way to use SymbolRenderer to draw legend symbol
    double width = 12;
    double height = 12;
    double radius = 2;

    if (symbolRenderer is LineSymbolRenderer) {
      width = 12;
      height = 3;
      radius = 2;
    } else if (symbolRenderer is RoundedRectSymbolRenderer) {
      width = 12;
      height = 12;
      radius = 2;
    } else if (symbolRenderer is RectSymbolRenderer) {
      width = 12;
      height = 12;
      radius = 0;
    } else if (symbolRenderer is CircleSymbolRenderer) {
      width = 12;
      height = 12;
      radius = 12;
    }

    final Widget symbol = ui.Container(
      decoration: ui.BoxDecoration(
        borderRadius: ui.BorderRadius.circular(radius),
        color: ColorUtil.toDartColor(legend?.color) ?? ui.Colors.transparent,
      ),
      width: width,
      height: height,
    );

    return ui.Row(
      mainAxisSize: ui.MainAxisSize.min,
      mainAxisAlignment: ui.MainAxisAlignment.center,
      children: [
        symbol,
        ui.Padding(
          padding:
              const ui.EdgeInsets.only(right: 16.0, top: 4, bottom: 4, left: 4),
          child: ui.Text(
            legend.label ?? '',
            style: ui.TextStyle(
              color: ColorUtil.toDartColor(legend.textStyle?.color),
              fontFamily: legend.textStyle?.fontFamily,
              fontSize: legend.textStyle?.fontSize?.toDouble(),
            ),
          ),
        )
      ],
    );
  }
}

// TODO: Expose settings for tabular layout.
/// Strategy that builds a tabular legend.
///
/// [legendEntryLayout] custom strategy for creating widgets for each legend
/// entry.
/// [legendLayout] custom strategy for creating legend widget from list of
/// widgets that represent a legend entry.
class TabularLegendContentBuilder extends BaseLegendContentBuilder {
  final LegendEntryLayout legendEntryLayout;
  final LegendLayout legendLayout;

  TabularLegendContentBuilder(
      {LegendEntryLayout legendEntryLayout, LegendLayout legendLayout})
      : this.legendEntryLayout =
            legendEntryLayout ?? const SimpleLegendEntryLayout(),
        this.legendLayout =
            legendLayout ?? new TabularLegendLayout.horizontalFirst();

  @override
  bool operator ==(Object o) {
    return o is TabularLegendContentBuilder &&
        legendEntryLayout == o.legendEntryLayout &&
        legendLayout == o.legendLayout;
  }

  @override
  int get hashCode => hashValues(legendEntryLayout, legendLayout);
}
